---
id: "0027"
datePosted: "2022-12-08"
title: "Outline an acceptable verification process for GPTE codebase."
lovelace:
gimbals:
status: "Open"
devCategory: "Verification"
bbk: [""]
approvalProcess: 4
multipleCommitments: false
repositoryLink: ""
---

## Outcome:


## Requirements:


## How To Start:


## Links + Tips:
- Testing = checking that it behaves as expected (eg adds up correctly)
- Auditing = checking for vulnerabilities, errors etc which may cause risk.
- It is possible to pass testing but fail auditing, and vice versa.


## How To Complete:
