---
id: "0021"
datePosted: "2022-11-03"
title: "Extend Contributor Token by Experimenting with CIP-68"
lovelace: 200000000
gimbals: 4000
status: "Coming Soon"
devCategory: "Contributor Token"
bbk: []
approvalProcess: 4
multipleCommitments: false
repositoryLink: "https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/completion-token/completion-and-contribution-token-proposal"
---

## Outcome:

## Requirements:

## How To Start:

## Links + Tips:
- https://cips.cardano.org/cips/cip68/

## How To Complete:
