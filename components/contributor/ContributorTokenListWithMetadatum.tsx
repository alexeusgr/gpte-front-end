import { useEffect, useState } from "react";
import { useQuery, gql } from "@apollo/client";
import {
  Box,
  Heading,
  Text,
  Center,
  Spinner,
  Grid,
  GridItem,
  Table,
  TableContainer,
  Thead,
  Tr,
  Th,
  Td,
  Tbody,
  Link,
  Badge,
} from "@chakra-ui/react";
import { treasury } from "../../cardano/plutus/treasuryContract";
import { hexToString } from "../../cardano/utils";
import { ContributorReputation, MasteryDatum } from "../../types/contributor";

const CONTRIBUTOR_TOKEN_QUERY = gql`
  query QueryReferenceAddress($contractAddress: String!) {
    utxos(where: { address: { _eq: $contractAddress } }) {
      tokens {
        asset {
          assetName
        }
      }
      datum {
        value
      }
    }
  }
`;

// To do: Make a type for refUtxos
const getAllContributorReferenceDatums = (refUtxos: any) => {
  const _contributorList: ContributorReputation[] = []
  refUtxos.map((utxo: any) => {
    const _masteryList: MasteryDatum[] = []
    utxo.datum.value.fields[0].map.map((cd: any) => {
      const _mastery: MasteryDatum = {
        description: hexToString(cd.k.bytes),
        value: cd.v.int
      }
      _masteryList.push(_mastery)
    })
    const _contributor: ContributorReputation = {
      alias: hexToString(utxo.datum.value.fields[2].bytes),
      completedCommitments: utxo.datum.value.fields[1].int,
      instructor: utxo.datum.value.fields[0].map[3]?.v.int == 1,
      masteryData: _masteryList
    }
    _contributorList.push(_contributor)
  })
  return _contributorList
}

// To do:
// [x] Create a TS Type for Contributor Info
// [ ] From GraphQL, populate an array of Typed objects
// [ ] Then render a more dynamic table. For example, filtering Instructors, sorting, color coding, etc.

const ContributorTokenListWithMetadatum = () => {
  //   const _policyID = treasury.accessTokenPolicyId;
  const _address = treasury.contributorReferenceAddress;

  const { data, loading, error } = useQuery(CONTRIBUTOR_TOKEN_QUERY, {
    variables: {
      contractAddress: _address,
    },
  });

  if (loading) {
    return (
      <Center p="10">
        <Spinner size="xl" speed="1.0s" />
      </Center>
    );
  }

  if (error) {
    console.error(error);
    return <Heading size="lg">Error loading data...</Heading>;
  }

  return (
    <Box mb="5" p="5" border="1px" borderRadius="lg">
      <Heading size="lg">List of Metadatum Contrib Tokens</Heading>
      <Text>
        <Link
          href="https://cardanoscan.io/address/71f387cf1456974344849458e7b4d63df5ca24e95d4f10d5dd56144096"
          target="_blank"
          >
          {treasury.contributorReferenceAddress}
        </Link>
      </Text>
      <TableContainer>
        <Table>
          <Thead>
            <Tr>
              <Th color="white">Token Alias</Th>
              <Th color="white">Mastery 301</Th>
              <Th color="white">Mastery 302</Th>
              <Th color="white">Mastery 303</Th>
              <Th color="white">Contributions</Th>
            </Tr>
          </Thead>
          <Tbody>
          {getAllContributorReferenceDatums(data.utxos).map((contrib: ContributorReputation) => (
              <Tr key={contrib.alias}>
                <Td>
                  {contrib.alias} {contrib.instructor && <Badge colorScheme='orange' size='xs'>Instructor</Badge> }
                </Td>
                {contrib.masteryData.slice(0,3).map((mastery: MasteryDatum) => (
                  <Td key={mastery.description}>{mastery.value}</Td>
                ))}
                <Td>
                  {contrib.completedCommitments}
                </Td>
              </Tr>
          ))}
          </Tbody>
        </Table>
      </TableContainer>
    </Box>
  );
};

export default ContributorTokenListWithMetadatum;
