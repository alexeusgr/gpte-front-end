import { useEffect, useState } from "react";
import { useQuery, gql } from "@apollo/client";
import { Box, Heading, Text, Center, Spinner, Grid, GridItem } from "@chakra-ui/react";
import { treasury } from "../../cardano/plutus/treasuryContract";
import { hexToString } from "../../cardano/utils";

const CONTRIBUTOR_TOKEN_QUERY = gql`
  query ListOfContribTokens($contribPolicyId: Hash28Hex!) {
    assets(where: { policyId: { _eq: $contribPolicyId } } ) {
      assetName
    }
  }
`;

const ContributorTokenListStatic = () => {
  const _policyID = treasury.accessTokenPolicyId;

  const { data, loading, error } = useQuery(CONTRIBUTOR_TOKEN_QUERY, {
    variables: {
      contribPolicyId: _policyID,
    },
  });

  if (loading) {
    return (
      <Center p="10">
        <Spinner size="xl" speed="1.0s" />
      </Center>
    );
  }

  if (error) {
    console.error(error);
    return <Heading size="lg">Error loading data...</Heading>;
  }

  return (
    <Box mb="5" p="5" border="1px" borderRadius="lg">
      <Heading size="lg">List of Static Contrib Tokens</Heading>
      <Text>{_policyID}</Text>
      <Grid templateColumns="repeat(2, 1fr)" gap={6} pt="3">
        {data.assets.map((asset: any, i: number) => (
            <GridItem key={i} bg="purple.900" p="2">{hexToString(asset.assetName)}</GridItem>
        ))}
      </Grid>
    </Box>
  );
};

export default ContributorTokenListStatic;
