import {
  Box,
  Heading,
  Text,
} from "@chakra-ui/react";

// To do: Implement this -> Write Project description
// Rewrite this component with a GraphQL Query that
// looks at Treasury Datum and returns the current number of Project Hashes.

const ProjectCountWidget = () => {
  return (
    <Box p="5" bg="gray.900" textAlign="center">
      <Heading size="3xl">13</Heading>
      <Text>Live Projects</Text>
    </Box>
  );
};

export default ProjectCountWidget;
