import { useState } from "react";
import { useQuery, gql } from "@apollo/client";
import { Box, Heading, Text, Center, Spinner } from "@chakra-ui/react";

const METADATA_QUERY = gql`
  query TransactionsWithMetadataKey($metadatakey: String!) {
    transactions(where: { metadata: { key: { _eq: $metadatakey } } }) {
      metadata {
        key
        value
      }
    }
  }
`;

type Props = {
  metadataKey: string;
};

const DistributionCountWidget: React.FC<Props> = ({ metadataKey }) => {
  const { data, loading, error } = useQuery(METADATA_QUERY, {
    variables: {
      metadatakey: metadataKey,
    },
  });

  if (loading) {
    return (
      <Center p="10">
        <Spinner size="xl" speed="1.0s" />
      </Center>
    );
  }

  if (error) {
    console.error(error);
    return <Heading size="lg">Error loading data...</Heading>;
  }

  // This is a Quick solution just to have live data on the page
  // To do: implement a "Number of distributions" function that can be used for real!
  return (
    <Box p="5" bg="gray.900" textAlign="center">
      <Heading size="3xl">{data.transactions.length / 2}</Heading>
      <Text>Completed Projects</Text>
    </Box>
  );
};

export default DistributionCountWidget;
