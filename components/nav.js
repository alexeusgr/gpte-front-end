import Link from "next/link";
import { Flex, Spacer, Text } from "@chakra-ui/react";
import { FaSchool, FaTools } from "react-icons/fa";
import { CardanoWallet } from "@meshsdk/react";

export default function Nav() {
  return (
    <Flex
      direction="row"
      w="100%"
      p="5"
      bg="gray.800"
      fontWeight="900"
      fontSize="lg"
      color="white"
    >
      <Text>
        <Link href="/">
          <FaSchool />
        </Link>
      </Text>
      <Spacer />
      <Text>
        <Link href="/projects">Contribute to Projects</Link>
      </Text>
      <Spacer />
      <Text>
        <Link href="/dashboard">Reporting and Data</Link>
      </Text>
      <Spacer />
      <Text>
        <Link href="/contributors">Contributor Tokens</Link>
      </Text>
      <Spacer />
      <Text>
        <Link href="/how-gpte-works">How it Works</Link>
      </Text>
      <Spacer />

      <CardanoWallet />
    </Flex>
  );
}
