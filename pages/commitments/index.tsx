import { Box, Heading, Grid, GridItem } from "@chakra-ui/react";
import { NextPage } from "next";
import { useQuery, gql } from "@apollo/client";
import SelectAndDistributeCommitmentUTxO from "../../components/escrow/SelectAndDistributeCommitmentUTxO";


const CommitmentsPage: NextPage = () => {
  return (
    <Box>
      <Heading>Commitments</Heading>
      <SelectAndDistributeCommitmentUTxO />
    </Box>
  );
};

export default CommitmentsPage;
