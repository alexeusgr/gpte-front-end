import { Box, Heading, Text, List, ListIcon, ListItem } from "@chakra-ui/react";
import { NextPage } from "next";
import { treasury } from "../../cardano/plutus/treasuryContract";
import ContributorTokenListStatic from "../../components/contributor/ContributorTokenListStatic";
import ContributorTokenListWithMetadatum from "../../components/contributor/ContributorTokenListWithMetadatum";

const ContributorsPage: NextPage = () => {
  return (
    <Box w="80%" mx="auto">
      <Heading pb="5">Contributor List</Heading>
      <ContributorTokenListWithMetadatum />
      {treasury.network == "0" && <ContributorTokenListStatic />}
    </Box>
  );
};

export default ContributorsPage;
